# Slovenian translation of comp-scale.
# This file is put in the public domain.
# r1to <renato.rener@gmail.com>, 2011.
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: comp-scale 1.0\n"
"Report-Msgid-Bugs-To: enlightenment-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2013-01-24 22:24-0200\n"
"PO-Revision-Date: 2011-03-28 12:34+0100\n"
"Last-Translator: Renato Rener <renato.rener@gmail.com>\n"
"Language-Team: Slovenian <sl@li.org>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Slovenian\n"
"X-Poedit-Country: Slovenia\n"
"X-Poedit-SourceCharset: utf-8\n"

#: src/e_mod_config.c:57
msgid "Scale Windows Module"
msgstr "Modul Scale windows"

#: src/e_mod_config.c:119
msgid "Current Desktop"
msgstr "Trenutno namizje"

#: src/e_mod_config.c:120 src/e_mod_config.c:153
msgid "Minimum space between windows"
msgstr "Najmanjši prostor med okni"

#: src/e_mod_config.c:122 src/e_mod_config.c:155
#, c-format
msgid "%1.0f"
msgstr "%1.0f"

#: src/e_mod_config.c:126 src/e_mod_config.c:159
msgid "Scale duration"
msgstr "Trajanje spremembe merila"

#: src/e_mod_config.c:128 src/e_mod_config.c:161 src/e_mod_config.c:200
#, c-format
msgid "%1.2f"
msgstr "%1.2f"

#: src/e_mod_config.c:133 src/e_mod_config.c:165
msgid "Slotted Layout"
msgstr "Režasti iygled"

#: src/e_mod_config.c:135 src/e_mod_config.c:167
msgid "Natural Layout"
msgstr "Naravni izgled"

#: src/e_mod_config.c:139 src/e_mod_config.c:169
msgid "Grow more!"
msgstr "Povečaj!"

#: src/e_mod_config.c:142 src/e_mod_config.c:172
msgid "Keep it tight!"
msgstr "Ohrani tesno!"

#: src/e_mod_config.c:145 src/e_mod_config.c:178
msgid "Show iconified windows"
msgstr ""

#: src/e_mod_config.c:152
msgid "Show All Desktops"
msgstr "Prikaži vsa namizja"

#: src/e_mod_config.c:175 src/e_mod_config.c:206
msgid "Fade in windows"
msgstr "Pojavljajoča okna"

#: src/e_mod_config.c:185 src/e_mod_config.c:209
msgid "Fade out shelves and popups"
msgstr "Pojemanje polic in pojavnih oken"

#: src/e_mod_config.c:188 src/e_mod_config.c:212
msgid "Darken desktop"
msgstr "Zatemni namizje"

#: src/e_mod_config.c:193 src/e_mod_main.c:277 src/e_mod_main.c:303
#: src/e_mod_main.c:305 src/e_mod_main.c:307 src/e_mod_main.c:309
#: src/e_mod_main.c:311 src/e_mod_main.c:313 src/e_mod_main.c:315
#: src/e_mod_main.c:374 src/e_mod_main.c:375 src/e_mod_main.c:376
#: src/e_mod_main.c:377 src/e_mod_main.c:378 src/e_mod_main.c:503
msgid "Scale Windows"
msgstr "Spremeni merilo okna"

#: src/e_mod_config.c:196
msgid "Pager Settings"
msgstr "Nastavitve preklopnika"

#: src/e_mod_config.c:198
msgid "Zoom duration"
msgstr "Trajanje spremembe merila"

#: src/e_mod_config.c:203
msgid "Overlap Shelves"
msgstr ""

#: src/e_mod_config.c:217
#, fuzzy
msgid "Pager"
msgstr "Spremeni merilo preklopnika"

#: src/e_mod_main.c:229
msgid "Look"
msgstr "Videz"

#: src/e_mod_main.c:231
msgid "Composite Scale Windows"
msgstr "Kompozitiraj okna ki spreminjajo merilo"

#: src/e_mod_main.c:305 src/e_mod_main.c:375
msgid "Scale Windows (All Desktops)"
msgstr "Spremeni merilo okna (vsa namizja)"

#: src/e_mod_main.c:307 src/e_mod_main.c:376
msgid "Scale Windows (By Class)"
msgstr "Spremeni merilo okna (po razredu)"

#: src/e_mod_main.c:309 src/e_mod_main.c:320 src/e_mod_main.c:377
#: src/e_mod_main.c:381
msgid "Select Next"
msgstr "Izberi naslednje"

#: src/e_mod_main.c:311 src/e_mod_main.c:322 src/e_mod_main.c:378
#: src/e_mod_main.c:382
msgid "Select Previous"
msgstr "Izberi prejšnje"

#: src/e_mod_main.c:313
msgid "Select Next (All)"
msgstr "Izberi naslednje (vse)"

#: src/e_mod_main.c:315
msgid "Select Previous (All)"
msgstr "Izberi prejšnje (vse)"

#: src/e_mod_main.c:318 src/e_mod_main.c:320 src/e_mod_main.c:322
#: src/e_mod_main.c:324 src/e_mod_main.c:326 src/e_mod_main.c:328
#: src/e_mod_main.c:330 src/e_mod_main.c:380 src/e_mod_main.c:381
#: src/e_mod_main.c:382 src/e_mod_main.c:383 src/e_mod_main.c:384
#: src/e_mod_main.c:385 src/e_mod_main.c:386 src/e_mod_main.c:515
msgid "Scale Pager"
msgstr "Spremeni merilo preklopnika"

#: src/e_mod_main.c:324 src/e_mod_main.c:383
msgid "Select Left"
msgstr "Izberi levo"

#: src/e_mod_main.c:326 src/e_mod_main.c:384
msgid "Select Right"
msgstr "Izberi desno"

#: src/e_mod_main.c:328 src/e_mod_main.c:385
msgid "Select Up"
msgstr "Izberi zgoraj"

#: src/e_mod_main.c:330 src/e_mod_main.c:386
msgid "Select Down"
msgstr "Izberi spodaj"

#: src/e_mod_main.c:509
#, fuzzy
msgid "Scale all Windows"
msgstr "Spremeni merilo okna"

#: src/e_mod_main.c:614
msgid "Settings"
msgstr "Nastavitve"

#~ msgid "Scale"
#~ msgstr "Spremeni merilo"

#~ msgid "Layout Options"
#~ msgstr "Možnosti izgleda"

#~ msgid "Test"
#~ msgstr "Testiraj"

#~ msgid "Desktop"
#~ msgstr "Namizje"
